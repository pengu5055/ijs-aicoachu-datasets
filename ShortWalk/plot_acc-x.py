import numpy as np
import matplotlib.pyplot as plt
import cmasher as cmr

colors = cmr.take_cmap_colors("cmr.torch", 20, cmap_range=(0.2, 0.8), return_fmt="hex")

leftankle = np.genfromtxt("left-ankle.csv", delimiter=",", skip_header=2, invalid_raise=False)
rightankle = np.genfromtxt("right-ankle.csv", delimiter=",", skip_header=2, invalid_raise=False)
pelvis = np.genfromtxt("pelvis.csv", delimiter=",", skip_header=2, invalid_raise=False)

ltime = (leftankle[:, 2] - leftankle[0, 2])/1000
lacc_x = leftankle[:, 3] / 1000

rtime = (rightankle[:, 2] - rightankle[0, 2])/1000
racc_x = rightankle[:, 3] / 1000

ptime = (pelvis[:, 2] - pelvis[0, 2])/1000
pacc_x = pelvis[:, 3] / 1000

plt.plot(ltime, lacc_x, color=colors[5], lw=1, label="Left")
plt.plot(rtime, racc_x, color=colors[10], lw=1, label="Right")
plt.plot(ptime, pacc_x, color=colors[19], lw=1, label="Pelvis")
plt.legend()
plt.title("A short walk..")
plt.xlabel("t [s]")
plt.ylabel("acc_x [g]")
plt.show()
